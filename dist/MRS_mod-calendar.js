function Calendar(obj){
    this.data = {};
    this.data.t0 = '<div class="cal_info">Выберите день:</div>';
    this.data.t1 = '<a href="" class="cal_item"> <span class="citem_type"><bdi></bdi></span> <div class="cal_separator"> <div class="cs_vert"></div></div><span class="citem_val"><bdi></bdi></span> </a>';
    this.data.t2 = '<div class="cal_switch"> <span class="citem_type"></span> <div class="cal_separator"> <div class="cs_vert"></div></div><i class="icon-left-dir swPrev"></i><span class="citem_val"></span><i class="icon-right-dir swNext"></i></div>';
    this.data.loader = '<img class="sap_loader" src="../static/css/blocks/social/loader-big.gif">';
    this.data.monthNames = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
    this.data.weekNames = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'];
    this.data.date = new Date();
    this.data.url = obj.url;
    this.data.postURL = obj.postURL;
    this.data.struct = {};
    this.data.timeStampsArr = [];
    this.data.slider = obj.sliderInstance;
    this.data.root = obj.node;
    this.data.resizeVal = 0;

    
    this.data.clickNum = 0;
    this.data.curMonth = this.data.date.getMonth()+1;
    this.data.curYear = this.data.date.getFullYear();
    this.data.curDate = this.data.date.getDate();    
    this.data.curSwMonth = 0;
    this.data.curSwYear = this.data.curYear;
    this.data.curSwDay = this.data.curDate;
    this.data.needMonth;
    this.data.needYear;
    this.data.selectedMonth = this.data.curMonth;
    this.data.selectedYear = this.data.curYear;
    this.data.selectedDate = this.data.curDate;


    var that = this;
    this.fetch();
    window.onresize = function(){that.resize();}

}

Calendar.prototype = {
    check: function(){        
	if(this.data.needMonth>12){
	    this.data.needMonth -=12;
	    this.data.needYear ++;
	    this.check();
	}
} ,
    draw: function(int){
    var t1Instance,t2Instance,t0Instance;
        this.data.curSwMonth = int;
        this.data.needMonth = this.data.curMonth+int;
        this.data.needYear = this.data.curYear;
        this.check(int);
        daysInMonth = this.getDaysInMonth(this.data.needMonth,this.data.needYear);

    $(this.data.root).empty();
    t0Instance = $(this.data.t0);
    $(this.data.root).append(t0Instance);

    for(var i=0;i<=daysInMonth;i++){
            t1Instance = $(this.data.t1);
            t2Instance = $(this.data.t2);
        if(i===0){
            t2Instance.find('.citem_type').text('МЕСЯЦ');
            t2Instance.find('.citem_val').text(this.data.monthNames[this.data.needMonth-1].toUpperCase());
            $(this.data.root).append(t2Instance);
        }
        else if(i===daysInMonth){
            t2Instance.find('.citem_type').text('ГОД');
            t2Instance.find('.citem_val').text(this.data.needYear);
            $(this.data.root).append(t2Instance);
            continue;
        }
            t1Instance.find('.citem_type').find('bdi').text(this.data.weekNames[(new Date(this.data.needYear,this.data.needMonth-1,i+1)).getDay()]);

            t1Instance.find('.citem_val').find('bdi').text(i+1);

            //прошлые дни
            if(i+1<this.data.curDate && this.data.needYear === this.data.curYear && this.data.needMonth === this.data.curMonth){
                t1Instance.removeAttr('href').addClass('past');
            }

            //активный день
            if(i+1 === this.data.selectedDate && this.data.needYear === this.data.selectedYear && this.data.needMonth === this.data.selectedMonth){
                t1Instance.addClass('active');
            }

            //присоединение
            $(this.data.root).append(t1Instance);

    }

    if(typeof this.resize === 'function'){
        this.resize();
    }
    if(typeof this.setArrows === 'function'){
        this.setArrows();
    }
    if(typeof this.specialDays === 'function'){
        this.specialDays();
    }
    if(typeof this.setEmptyClicks === 'function'){
        this.setEmptyClicks();
    }

    //открытие первого дня при первом запуске
    if(!this.data.firstTime){
        $(this.data.root).find('.cal_item').filter('.active').click();
    }

} ,
    fetch: function(){
    $(this.data.root).append($(this.data.loader));
    if(typeof this.data.url === 'object'){
        this.data.struct = this.data.url;
        this.data.timeStampsArr = Object.keys(this.data.struct);            
        this.draw(0);
    }
    else if(typeof this.data.url === 'string'){
        $.ajax({
            type: 'GET',
            url: this.data.url,
            dataType: 'json',
            success: function(data){
                this.data.struct = data;
                this.data.timeStampsArr = Object.keys(this.data.struct);
                this.draw(0);
            }
        });
    }
    else throw('error: wrong URL');
} ,
    getDaysInMonth: function(month,year){
    return new Date(year, month, 0).getDate();
} ,
    loadMRS: function(arr){
    var that = this;
    if(arr){
        this.data.slider.delete('all',function(){
            that.data.slider.add(arr);
        });  
    }
    else {
        this.data.slider.delete('all');
    }
          
} ,
    resize: function(){
    this.data.resizeVal = ($(window).innerWidth() - 1280)/100;
    if(this.data.resizeVal > 0){
        $('.cal_item').css('margin-right',this.data.resizeVal+3+'px');
        $('.cal_switch').eq(0).css('margin-right',this.data.resizeVal+3+'px');
        $('.cal_item').find('span').css('padding','5px '+(this.data.resizeVal+2)+'px');
    }
    else {
        $('.cal_item').css('margin-right','3px');
        $('.cal_switch').eq(0).css('margin-right','3px');
        $('.cal_item').find('span').css('padding','5px 2px');
    }
} ,
    setArrows: function(){
    var that = this;
    $(this.data.root).find('.swPrev').eq(0).on('click',function(){
        if(that.data.curSwMonth === 0) return;
        else {
            that.data.curSwMonth--;
            that.draw(that.data.curSwMonth);
        }
    });

    $(this.data.root).find('.swNext').eq(0).on('click',function(){
        that.data.curSwMonth++;
        that.draw(that.data.curSwMonth);
    });

    $(this.data.root).find('.swPrev').eq(1).on('click',function(){
        if((that.data.curSwMonth-12) < 0) {
            that.data.curSwYear = that.data.curYear;
            that.draw(0);
        }
        else {
            that.data.curSwYear--;
            that.draw(that.data.curSwMonth-12);
        }
    });

    $(this.data.root).find('.swNext').eq(1).on('click',function(){
        that.data.curSwYear++;
        that.draw(that.data.curSwMonth+12);
    });
}    ,
    setEmptyClicks: function(){
    var that = this;
    $(this.data.root).find('.cal_item').not('.past,.not_empty').on('click',function(e){
        e.preventDefault();

        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
        that.data.selectedDate = $(this).find('.citem_val').find('bdi').text()*1;
        that.data.selectedYear = that.data.needYear;
        that.data.selectedMonth = that.data.needMonth;

        that.loadMRS();
        // console.log('curYear',that.data.curYear);
        // console.log('needYear',that.data.needYear);
        // console.log('curSwYear',that.data.curSwYear);
    });
} ,
    specialDays: function(){
    var date,that=this;
    for(var i=0;i<this.data.timeStampsArr.length;i++){
        date = new Date(this.data.timeStampsArr[i]*1000);
        if(date.getFullYear() === this.data.needYear){
            if(date.getMonth()+1 === this.data.needMonth){
                $(this.data.root).find('.cal_item').eq(date.getDate()-1).addClass('not_empty').on('click',{i: i},function(e){
                    e.preventDefault();

                    $(this).parent().find('.active').removeClass('active');
                    $(this).addClass('active'); 
                    that.data.selectedDate = $(this).find('.citem_val').find('bdi').text()*1;
                    that.data.selectedYear = that.data.needYear;
                    that.data.selectedMonth = that.data.needMonth;

                    //console.log(that.data.struct[that.data.timeStampsArr[e.data.i]]);
                    that.loadMRS(that.data.struct[that.data.timeStampsArr[e.data.i]]);
                });
            }
        }
    }
} ,
    save: function(){
	var data = this.data.slider.getData(),
		stamp = Math.floor((new Date).getTime()/1000),
		that = this,
		obj = {};

	obj[stamp] = data;

	$.ajax({
		type: 'POST',
		data: obj,
		url: that.data.postURL,
		dataType: 'json',
		success: function(data){
			//do logick
		}
	});
}
}