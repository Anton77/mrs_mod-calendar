function Calendar(obj){
    this.data = {};
    this.data.t0 = '<div class="cal_info">Выберите день:</div>';
    this.data.t1 = '<a href="" class="cal_item"> <span class="citem_type"><bdi></bdi></span> <div class="cal_separator"> <div class="cs_vert"></div></div><span class="citem_val"><bdi></bdi></span> </a>';
    this.data.t2 = '<div class="cal_switch"> <span class="citem_type"></span> <div class="cal_separator"> <div class="cs_vert"></div></div><i class="icon-left-dir swPrev"></i><span class="citem_val"></span><i class="icon-right-dir swNext"></i></div>';
    this.data.loader = '<img class="sap_loader" src="../static/css/blocks/social/loader-big.gif">';
    this.data.monthNames = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
    this.data.weekNames = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'];
    this.data.date = new Date();
    this.data.url = obj.url;
    this.data.postURL = obj.postURL;
    this.data.struct = {};
    this.data.timeStampsArr = [];
    this.data.slider = obj.sliderInstance;
    this.data.root = obj.node;
    this.data.resizeVal = 0;

    
    this.data.clickNum = 0;
    this.data.curMonth = this.data.date.getMonth()+1;
    this.data.curYear = this.data.date.getFullYear();
    this.data.curDate = this.data.date.getDate();    
    this.data.curSwMonth = 0;
    this.data.curSwYear = this.data.curYear;
    this.data.curSwDay = this.data.curDate;
    this.data.needMonth;
    this.data.needYear;
    this.data.selectedMonth = this.data.curMonth;
    this.data.selectedYear = this.data.curYear;
    this.data.selectedDate = this.data.curDate;


    var that = this;
    this.fetch();
    window.onresize = function(){that.resize();}

}

Calendar.prototype = {
    @import 'check.js' ,
    @import 'draw.js' ,
    @import 'fetch.js' ,
    @import 'getDaysInMonth.js' ,
    @import 'loadMRS.js' ,
    @import 'resize.js' ,
    @import 'setArrows.js' ,
    @import 'setEmptyClicks.js' ,
    @import 'specialDays.js' ,
    @import 'save.js'
}