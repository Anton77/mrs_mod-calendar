draw: function(int){
    var t1Instance,t2Instance,t0Instance;
        this.data.curSwMonth = int;
        this.data.needMonth = this.data.curMonth+int;
        this.data.needYear = this.data.curYear;
        this.check(int);
        daysInMonth = this.getDaysInMonth(this.data.needMonth,this.data.needYear);

    $(this.data.root).empty();
    t0Instance = $(this.data.t0);
    $(this.data.root).append(t0Instance);

    for(var i=0;i<=daysInMonth;i++){
            t1Instance = $(this.data.t1);
            t2Instance = $(this.data.t2);
        if(i===0){
            t2Instance.find('.citem_type').text('МЕСЯЦ');
            t2Instance.find('.citem_val').text(this.data.monthNames[this.data.needMonth-1].toUpperCase());
            $(this.data.root).append(t2Instance);
        }
        else if(i===daysInMonth){
            t2Instance.find('.citem_type').text('ГОД');
            t2Instance.find('.citem_val').text(this.data.needYear);
            $(this.data.root).append(t2Instance);
            continue;
        }
            t1Instance.find('.citem_type').find('bdi').text(this.data.weekNames[(new Date(this.data.needYear,this.data.needMonth-1,i+1)).getDay()]);

            t1Instance.find('.citem_val').find('bdi').text(i+1);

            //прошлые дни
            if(i+1<this.data.curDate && this.data.needYear === this.data.curYear && this.data.needMonth === this.data.curMonth){
                t1Instance.removeAttr('href').addClass('past');
            }

            //активный день
            if(i+1 === this.data.selectedDate && this.data.needYear === this.data.selectedYear && this.data.needMonth === this.data.selectedMonth){
                t1Instance.addClass('active');
            }

            //присоединение
            $(this.data.root).append(t1Instance);

    }

    if(typeof this.resize === 'function'){
        this.resize();
    }
    if(typeof this.setArrows === 'function'){
        this.setArrows();
    }
    if(typeof this.specialDays === 'function'){
        this.specialDays();
    }
    if(typeof this.setEmptyClicks === 'function'){
        this.setEmptyClicks();
    }

    //открытие первого дня при первом запуске
    if(!this.data.firstTime){
        $(this.data.root).find('.cal_item').filter('.active').click();
    }

}