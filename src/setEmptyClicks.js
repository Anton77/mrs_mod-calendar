setEmptyClicks: function(){
    var that = this;
    $(this.data.root).find('.cal_item').not('.past,.not_empty').on('click',function(e){
        e.preventDefault();

        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
        that.data.selectedDate = $(this).find('.citem_val').find('bdi').text()*1;
        that.data.selectedYear = that.data.needYear;
        that.data.selectedMonth = that.data.needMonth;

        that.loadMRS();
        // console.log('curYear',that.data.curYear);
        // console.log('needYear',that.data.needYear);
        // console.log('curSwYear',that.data.curSwYear);
    });
}