resize: function(){
    this.data.resizeVal = ($(window).innerWidth() - 1280)/100;
    if(this.data.resizeVal > 0){
        $('.cal_item').css('margin-right',this.data.resizeVal+3+'px');
        $('.cal_switch').eq(0).css('margin-right',this.data.resizeVal+3+'px');
        $('.cal_item').find('span').css('padding','5px '+(this.data.resizeVal+2)+'px');
    }
    else {
        $('.cal_item').css('margin-right','3px');
        $('.cal_switch').eq(0).css('margin-right','3px');
        $('.cal_item').find('span').css('padding','5px 2px');
    }
}