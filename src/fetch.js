fetch: function(){
    $(this.data.root).append($(this.data.loader));
    if(typeof this.data.url === 'object'){
        this.data.struct = this.data.url;
        this.data.timeStampsArr = Object.keys(this.data.struct);            
        this.draw(0);
    }
    else if(typeof this.data.url === 'string'){
        $.ajax({
            type: 'GET',
            url: this.data.url,
            dataType: 'json',
            success: function(data){
                this.data.struct = data;
                this.data.timeStampsArr = Object.keys(this.data.struct);
                this.draw(0);
            }
        });
    }
    else throw('error: wrong URL');
}