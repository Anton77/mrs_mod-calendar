setArrows: function(){
    var that = this;
    $(this.data.root).find('.swPrev').eq(0).on('click',function(){
        if(that.data.curSwMonth === 0) return;
        else {
            that.data.curSwMonth--;
            that.draw(that.data.curSwMonth);
        }
    });

    $(this.data.root).find('.swNext').eq(0).on('click',function(){
        that.data.curSwMonth++;
        that.draw(that.data.curSwMonth);
    });

    $(this.data.root).find('.swPrev').eq(1).on('click',function(){
        if((that.data.curSwMonth-12) < 0) {
            that.data.curSwYear = that.data.curYear;
            that.draw(0);
        }
        else {
            that.data.curSwYear--;
            that.draw(that.data.curSwMonth-12);
        }
    });

    $(this.data.root).find('.swNext').eq(1).on('click',function(){
        that.data.curSwYear++;
        that.draw(that.data.curSwMonth+12);
    });
}   