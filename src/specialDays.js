specialDays: function(){
    var date,that=this;
    for(var i=0;i<this.data.timeStampsArr.length;i++){
        date = new Date(this.data.timeStampsArr[i]*1000);
        if(date.getFullYear() === this.data.needYear){
            if(date.getMonth()+1 === this.data.needMonth){
                $(this.data.root).find('.cal_item').eq(date.getDate()-1).addClass('not_empty').on('click',{i: i},function(e){
                    e.preventDefault();

                    $(this).parent().find('.active').removeClass('active');
                    $(this).addClass('active'); 
                    that.data.selectedDate = $(this).find('.citem_val').find('bdi').text()*1;
                    that.data.selectedYear = that.data.needYear;
                    that.data.selectedMonth = that.data.needMonth;

                    //console.log(that.data.struct[that.data.timeStampsArr[e.data.i]]);
                    that.loadMRS(that.data.struct[that.data.timeStampsArr[e.data.i]]);
                });
            }
        }
    }
}